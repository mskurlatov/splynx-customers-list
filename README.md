Splynx Customer List Add-on
======================

Based On Splynx Add-on Skeleton

INSTALLATION
------------

Install add-on base:
~~~
cd /var/www/splynx/addons/
git clone git@bitbucket.org:splynx/splynx-addon-base-2.git
cd splynx-addon-base-2
composer install
~~~

Install Splynx Add-on Skeleton:
~~~
cd /var/www/splynx/addons/
git clone git@bitbucket.org:splynx/splynx-addon-skeleton.git
cd splynx-addon-skeleton
composer install
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-addon-skeleton/web/ /var/www/splynx/web/skeleton
~~~

Create Nginx config file:
~~~
sudo nano /etc/nginx/sites-available/splynx-skeleton.addons
~~~

with following content:
~~~
location /skeleton
{
        try_files $uri $uri/ /skeleton/index.php?$args;
}
~~~

Restart Nginx:
~~~
sudo service nginx restart
~~~

You can then access Splynx Customer List Add-on at the following address:
http://165.232.67.203/skeleton/
