<?php

namespace app\controllers;

use app\models\Customer;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii;
use yii\base\UserException;
use yii\helpers\ArrayHelper;    

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
          //              'roles' => ['@']
                    ]
                ]
            ]
        ];
    }
 
    public function actionIndex() {
/*        $this->getView()->title = 'Splynx Add-on Skeleton v2';        
        $this->getView()->params['breadcrumbs'][] = $this->getView()->title;        */
                       
        $filterModel = new \app\models\CustomerSearch();
        $filterModel->load(Yii::$app->request->queryParams);
        $dataProvider =  $filterModel->search($filterModel);
        
        return $this->render('index.twig', [
            "dataProvider" => $dataProvider,
            "filterModel" => $filterModel,
        ]);
    }
    
    public function actionEdit($id) {
        $model = Customer::findIdentity($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }
        return $this->render('update.twig', [
            'model' => $model,
        ]);
    }
}
