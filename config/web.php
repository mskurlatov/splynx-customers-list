<?php

return function ($params, $baseDir) {
    return [
        'components' => [
            'request' => [
                'baseUrl' => '/skeleton',
                'enableCookieValidation' => false
            ],
            'user' => [
                'identityClass' => 'splynx\models\Admin',
                'idParam' => 'splynx_admin_id',
                'loginUrl' => '/admin/login/?return=%2Fskeleton%2F',
                'enableAutoLogin' => false,
            ],
            'view' => [
            'class' => 'yii\web\View',
                'renderers' => [
                    'twig' => [
                        'class' => 'yii\twig\ViewRenderer',
                        'cachePath' => '@runtime/Twig/cache',
                        // Array of twig options:
                        'options' => [
                            'auto_reload' => true,
                        ],
                        'globals' => ['html' => '\yii\helpers\Html'],
                        'uses' => ['yii\bootstrap'],
                    ],
                    // ...
                ],
            ],            
        ],
    ];
};
