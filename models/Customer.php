<?php

namespace app\models;

use splynx\v2\models\customer\BaseCustomer;

/**
 * Class Customer
 * @package app\models
 */
class Customer extends BaseCustomer
{
    /**
     * Customers to be shown in the main grid
     * @return Customer[]|null
     */
    public function getCustomersToView() {
        return $this->findAll([]);
    }
}
