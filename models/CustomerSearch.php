<?php

namespace app\models;

use yii\base\Model;
use yii\data\ArrayDataProvider;
use app\models\Customer;

class CustomerSearch extends Customer
{
    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id'], 'integer'],
            [['name', 'status'], 'safe']
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($filterModel) {                
        // add conditions that should always apply here
        $params = [];
        if ($filterModel['id']) {
            $params['id'] = $filterModel['id'];
        }        
        if ($filterModel['name']) {
            $params['name'] = [
                'LIKE', $filterModel['name'],
            ];
        }                    
        if ($filterModel['status']) {
            $params['status'] = $filterModel['status'];
        }
        
        $list = (new Customer()) -> findall($params);
        $dataProvider = new \app\models\СustomerDataProvider();
        $dataProvider->sort = [
            'attributes' => ['id', 'name', 'status']
            ];
        $dataProvider->loadandfilter($list);
        return $dataProvider;    
    }            
}
