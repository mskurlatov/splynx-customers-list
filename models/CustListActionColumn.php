<?php

namespace app\models;

use yii;
use yii\helpers\Html;
use yii\helpers\Url;

class CustListActionColumn extends yii\grid\Column
{
    //public $headerOptions = ['class' => 'payment-receipt-action-column'];

    public $template = '{edit} {services}';

    public $buttons = [
        
    ];

    public function init()
    {
        parent::init();
        $this->initDefaultButtons();
    }

    protected function initDefaultButtons()
    {
        if (!isset($this->buttons['edit'])) {
            $this->buttons['edit'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Edit'),
                    'aria-label' => Yii::t('app', 'Edit'),
                    'data-pjax' => '0',
                ]);
                return Html::a('<span class="glyphicon glyphicon-file"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['services'])) {
            $this->buttons['services'] = function ($url, $model, $key) {
                $options = array_merge([
                    'title' => Yii::t('app', 'Services'),
                    'aria-label' => Yii::t('app', 'Services'),
                    'data-pjax' => '0',
                ]);
                return Html::a('<span class="glyphicon glyphicon-download"></span>', $url, $options);
            };
        }
    }

    public function createUrl($action, $model)
    {
        if ($action == 'edit') {
            $params = ['edit', 'id' => $model->id];
        }

        if ($action == 'services') {
            $params = ['show-services', 'cust_id' => $model->id];
        }

        return Url::toRoute($params);
    }

    protected function renderDataCellContent($model, $key, $index)
    {
        return preg_replace_callback('/\\{([\w\-\/]+)\\}/', function ($matches) use ($model, $key, $index) {
            $name = $matches[1];

            if (isset($this->buttons[$name])) {
                $url = $this->createUrl($name, $model);
                return call_user_func($this->buttons[$name], $url, $model, $key);
            } else {
                return '';
            }
        }, $this->template);
    }
}
