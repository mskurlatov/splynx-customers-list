<?php

namespace app\models;
use yii\data\ArrayDataProvider;
//use splynx\v2\models\customer\BaseCustomer;



class СustomerDataProvider extends ArrayDataProvider {
    
    public function loadAndFilter($models)
    {
        if (empty($models)) {
            $this->allModels = [];
        }       
        krsort($models);        
        $this->allModels = $models;
    }
}
